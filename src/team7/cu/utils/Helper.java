package team7.cu.utils;

import team7.cu.sims.Student;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Chitholian on 6/23/2017.
 */
public class Helper {
    /* ********* Constants ********* */
    public final static String AVATAR_LOCATION = "cu_sim_data/avatars/";

    /* ******** If Database is Used ********/
    public static Connection dbConnection;
    public static String dbName = "cu_sis";
    public static String dbUser = "chitholian";
    public static String dbAddress = "localhost:3306";

    /* ******* Check Files ******** */
    public static void checkFiles() {
        File file = new File(AVATAR_LOCATION);
        file.mkdirs();
    }

    /**
     * Checks if a target string contains of the words in the keyword string.
     *
     * @param keywords space separated words.
     * @param student  a log string where to search for keywords.
     * @return true if the target contains any of the words in the keywords or the keywords is null, false otherwise.
     */
    public static boolean searchOK(String keywords, Student student) {
        if (keywords == null) return true;
        else if (student == null) return false;
        String target = "";
        target += student.id + ",";
        target += student.name + ",";
        target += student.father + ",";
        target += student.mother + ",";
        target += student.session + ",";
        target += student.currentAddress + ",";
        target += student.permanentAddress + ",";
        target += student.religion + ",";
        target += student.gender + ",";
        target += student.phone + ",";
        target += student.email + ",";

        // To ignore case problems turn both into lowercase.
        target = target.toLowerCase();
        keywords = keywords.toLowerCase();
        // System.out.println(keywords + " ---- " + target);

        // replace comma and other whitespaces with single space, then split by single space.
        String[] tokens = keywords.replaceAll("[,\\s]+", " ").split(" ");
        for (String term : tokens)
            if (target.contains(term)) return true;
        return false;
    }

    /**
     * Checks if a date is possible
     *
     * @param year  Year Value i.e. 1997
     * @param month Month number in the year; 1 for January, 2 for February, 12 for December etc;
     * @param day   Number of the day in month; range [1, 31]
     * @return true if the date is possible, false otherwise
     */
    public static boolean isValidDate(int year, int month, int day) {
        // Check basic facts
        if (year < 0 || month < 1 || day < 1 || month > 12 || day > 31) return false;

        // February and Leap Year fact
        if (month == 2) {
            return day < 29 || isLeapYear(year) && day < 30;
        }

        // April and June fact (February is already checked)
        if (month < 8) // for April, June
            return day != 31 || month % 2 == 1;

        // September to December fact
        return day != 31 || month % 2 == 0;
    }

    /**
     * Checks if a year is leap year
     *
     * @param year Year to work on
     * @return true if the year is leap year, false otherwise
     */
    public static boolean isLeapYear(int year) {
        return year >= 0 && year % 4 == 0;
    }

    /* **************************** */

    public static void loginWith(String pass) throws SQLException, ClassNotFoundException {
        // Class.forName("com.mysql.jdbc.Driver");

        if (dbConnection != null) dbConnection.close();
        dbConnection = DriverManager.getConnection("jdbc:mysql://" + dbAddress + "/" + dbName, dbUser, pass);
        String fac_table_sql = "create table if not exists faculties(" +
                "    fac_id int auto_increment," +
                "    name varchar(150) not null," +
                "    primary key(fac_id));";

        String dept_table_sql = "create table if not exists departments(" +
                "    dept_id int auto_increment," +
                "    fac_id int NOT NULL," +
                "    short_name varchar(10)," +
                "    full_name varchar(150) not null," +
                "    primary key(dept_id)," +
                "    FOREIGN KEY (fac_id) REFERENCES faculties(fac_id) ON DELETE CASCADE);";

        String std_table_sql = "create table if not exists students(" +
                "    std_id numeric(10, 0) not null," +
                "    name varchar(64) not null," +
                "    father varchar(64) not null," +
                "    mother varchar(64)," +
                "    std_session varchar(11) not null," +
                "    curr_address text," +
                "    perm_address text," +
                "    phone varchar(20)," +
                "    email varchar(64)," +
                "    religion varchar(16)," +
                "    gender enum('Male', 'Female', 'Other') not null," +
                "    birth DATE not null," +
                "    dept_id int not null," +
                "    primary key(std_id)," +
                "    foreign key(dept_id) references departments(dept_id) ON DELETE CASCADE);";

        Statement statement = dbConnection.createStatement();
        statement.execute(fac_table_sql);
        statement.execute(dept_table_sql);
        statement.execute(std_table_sql);
        statement.close();
    }

    public static Connection getConnection() {
        return dbConnection;
    }
}
