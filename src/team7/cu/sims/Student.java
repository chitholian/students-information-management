package team7.cu.sims;

import team7.cu.utils.Helper;
import team7.cu.utils.MyDate;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

public class Student {
    public String name, session, father, mother, currentAddress, permanentAddress, phone, email;
    public String religion, gender;
    public int id;
    public Department dept;
    public MyDate birth;

    public Student(int id) {
        this.id = id;
    }

    public static ArrayList<Student> getAll() {
        ArrayList<Student> students = new ArrayList<>();

        try {
            Connection connection = Helper.getConnection();
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM students ORDER BY std_id DESC");
            while (set.next()) {
                students.add(buildFrom(set));
            }
            set.close();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return students;
    }

    public static Student findById(int id) {
        try {
            Student student = null;
            Connection connection = Helper.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM students WHERE std_id = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            if (set.first())
                student = buildFrom(set);
            set.close();
            statement.close();
            return student;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Student buildFrom(ResultSet set) {
        try {
            Student student = new Student(set.getInt("std_id"));
            student.session = set.getString("std_session");
            student.birth = MyDate.build(set.getString("birth"));
            student.name = set.getString("name");
            student.dept = Department.getById(set.getInt("dept_id"));
            student.currentAddress = set.getString("curr_address");
            student.email = set.getString("email");
            student.father = set.getString("father");
            student.gender = set.getString("gender");
            student.mother = set.getString("mother");
            student.permanentAddress = set.getString("perm_address");
            student.phone = set.getString("phone");
            student.religion = set.getString("religion");
            return student;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized static void delete(HashSet<Integer> selectedIds) {
        Connection connection = Helper.getConnection();
        File image;
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM students WHERE std_id = ?");
            for (Integer id : selectedIds) {
                statement.setInt(1, id);
                statement.execute();
                if ((image = new File(Helper.AVATAR_LOCATION + id + ".png")).exists()) image.delete();
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Search students by constraints
     *
     * @param constraints constraints to match
     * @return ArrayList of Student objects that match the constraints
     */
    public static ArrayList<Student> getAll(SearchConstraints constraints) {
        ArrayList<Student> list = new ArrayList<>();
        Cursor cursor = new Cursor(constraints);
        Student student;
        while ((student = cursor.fetch()) != null) list.add(student);
        cursor.close();

        // apply sorting
        Comparator<Student> comparator = new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                if (constraints.sortBy.equals(SortBy.NAME))
                    return o1.name.compareTo(o2.name);
                else if (constraints.sortBy.equals(SortBy.DEPARTMENT))
                    return o1.dept.fullName.compareTo(o2.dept.fullName);
                else if (constraints.sortBy.equals(SortBy.SESSION))
                    return o1.session.compareTo(o2.session);
                else if (constraints.sortBy.equals(SortBy.BIRTH))
                    return o1.birth.toUnixFormat().compareTo(o2.birth.toUnixFormat());
                else return o1.id - o2.id;
            }
        };
        // sort now
        if (constraints.sortOpt.equals(SortOpt.ASC))
            list.sort(comparator);
        else list.sort(comparator.reversed());

        // return the list
        return list;
    }

    public static Cursor search(SearchConstraints constraints) {
        return new Cursor(constraints);
    }

    public synchronized void save() {
        try {
            Connection connection = Helper.getConnection();
            if (findById(id) == null) {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO students VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
                statement.setInt(1, id);
                statement.setString(2, name);
                statement.setString(3, father);
                statement.setString(4, mother);
                statement.setString(5, session);
                statement.setString(6, currentAddress);
                statement.setString(7, permanentAddress);
                statement.setString(8, phone);
                statement.setString(9, email);
                statement.setString(10, religion);
                statement.setString(11, gender);
                statement.setString(12, birth.toUnixFormat());
                statement.setInt(13, dept.getId());
                statement.execute();
            } else {
                PreparedStatement statement = connection.prepareStatement("UPDATE students SET name = ?, father = ?, mother = ?, std_session = ?," +
                        "curr_address = ?, perm_address = ?, phone = ?, email = ?, religion = ?, gender = ?, birth = ?, dept_id = ? WHERE std_id = ?");
                statement.setInt(13, id);
                statement.setString(1, name);
                statement.setString(2, father);
                statement.setString(3, mother);
                statement.setString(4, session);
                statement.setString(5, currentAddress);
                statement.setString(6, permanentAddress);
                statement.setString(7, phone);
                statement.setString(8, email);
                statement.setString(9, religion);
                statement.setString(10, gender);
                statement.setString(11, birth.toUnixFormat());
                statement.setInt(12, dept.getId());
                statement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public enum SearchOption {
        ALL, BEFORE, AFTER, BETWEEN, EXACTLY
    }

    public enum SortBy {
        ID, NAME, DEPARTMENT, SESSION, BIRTH
    }

    public enum SortOpt {
        ASC, DESC
    }

    /**
     * This controls fetching students one by one
     */
    public static class Cursor {
        private ArrayList<Student> students;
        private SearchConstraints searchConstraints;
        private int index = 0;

        private Cursor(SearchConstraints constraints) {
            searchConstraints = constraints;
            students = Student.getAll();
        }

        /**
         * Get next student matching the constraints
         *
         * @return Instance of Student if exists satisfying the constraints null otherwise
         */
        public Student fetch() {
            while (index < students.size()) {
                Student student = students.get(index++);
                if (student == null) continue;
                if (searchConstraints == null) return student;
                // check department
                if (searchConstraints.department != null && student.dept.getId() != searchConstraints.department.getId())
                    continue;
                // check birth date
                if (searchConstraints.dob == SearchOption.BEFORE &&
                        searchConstraints.dobStart.toUnixFormat().compareTo(student.birth.toUnixFormat()) < 0)
                    continue;
                if (searchConstraints.dob == SearchOption.AFTER &&
                        searchConstraints.dobStart.toUnixFormat().compareTo(student.birth.toUnixFormat()) > 0)
                    continue;
                if (searchConstraints.dob == SearchOption.BETWEEN &&
                        (searchConstraints.dobStart.toUnixFormat().compareTo(student.birth.toUnixFormat()) > 0 ||
                                searchConstraints.dobEnd.toUnixFormat().compareTo(student.birth.toUnixFormat()) < 0))
                    continue;

                // check session
                if (searchConstraints.session == SearchOption.BEFORE && student.session.compareTo(searchConstraints.sessionStart) > 0)
                    continue;
                if (searchConstraints.session == SearchOption.AFTER && student.session.compareTo(searchConstraints.sessionStart) < 0)
                    continue;
                if (searchConstraints.session == SearchOption.BETWEEN &&
                        (student.session.compareTo(searchConstraints.sessionStart) < 0 ||
                                student.session.compareTo(searchConstraints.sessionEnd) > 0))
                    continue;
                if (searchConstraints.session == SearchOption.EXACTLY && student.session.compareTo(searchConstraints.sessionStart) != 0)
                    continue;

                // Ow! you came here, preconditions passed indeed.
                // Now apply our keyword search algorithm.
                if (Helper.searchOK(searchConstraints.keyword, student)) // Ok, all conditions passed.
                    return student;
            }
            return null;
        }

        public void close() {

        }
    }

    public final static class SearchConstraints {
        public String keyword, sessionStart, sessionEnd;
        public MyDate dobStart, dobEnd;
        public Department department;
        public Student.SortBy sortBy;
        public SortOpt sortOpt;
        public SearchOption dob, session;
    }
}
