package team7.cu.sims;

import team7.cu.comps.Dialogs;
import team7.cu.comps.MyLabel;
import team7.cu.comps.ScrollableList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

/**
 * Created by Chitholian on 6/23/2017.
 */
public class FacultyList extends ScrollableList {
    public FacultyList() {
        super();
        decorate();
        title = "List of Available Faculties";
    }

    @Override
    protected void init() {
        super.init();
        // Set cell renderer
        list.setCellRenderer(new FacListRenderer());
        // list.setVisibleRowCount(15);
        // Set action listener to the buttons
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addBtn.setEnabled(false);
                new Dialogs.Builder((MainFrame) container).onDispose(new Dialogs.DisposeListener() {
                    @Override
                    public void listen() {
                        addBtn.setEnabled(true);
                    }
                }).setPanel(new AddFacForm()).build(addBtn).setVisible(true);
            }
        });

        editBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddFacForm addFacForm = new AddFacForm();
                addFacForm.populateFrom((Faculty) list.getSelectedValue());
                new Dialogs.Builder((MainFrame) container).setPanel(addFacForm).build(editBtn).setVisible(true);
            }
        });

        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!Dialogs.confirm(deleteBtn, "Are you sure to delete permanently?")) return;
                HashSet<Integer> selectedIds = new HashSet<>();
                for (Object fac : list.getSelectedValuesList()) {
                    selectedIds.add(((Faculty) fac).getId());
                }
                Faculty.delete(selectedIds);
                refresh();
                FacultyList.super.notifyDataChanged(new DataChangeOption(DataChangeOption.FACULTY, System.currentTimeMillis()));
            }
        });

        refreshBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refresh();
            }
        });
    }

    @Override
    public void decorate() {
        gc.gridwidth = 4;
        insert(scrollPane);
        insert(emptyText);
        gc.gridy++;
        gc.gridwidth = 1;
        insert(addBtn);
        gc.gridx++;
        insert(deleteBtn);
        gc.gridx++;
        insert(editBtn);
        gc.gridx++;
        insert(refreshBtn);
    }

    @Override
    public void refresh() {
        ArrayList<Faculty> faculties = Faculty.getAll();
        if (faculties.size() == 0) {
            triggerEmptyList(); // Disable edit & delete button, show empty text, hide the list.
            return;
        }
        triggerNonEmptyList(); // show the list
        // Sort by name
        faculties.sort(new Comparator<Faculty>() {
            @Override
            public int compare(Faculty o1, Faculty o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        listModel.removeAllElements();
        for (Faculty faculty : faculties) {
            listModel.addElement(faculty);
        }
        list.updateUI();
        scrollPane.updateUI();
    }

    @Override
    public void notifyDataChanged(DataChangeOption changeOption) {
        refresh();
        super.notifyDataChanged(changeOption);
    }

    /* *********************** */
    private class FacListRenderer extends MyLabel implements ListCellRenderer<Object> {
        public FacListRenderer() {
            setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            setText(value.toString());
            setFont(new Font(Font.DIALOG, Font.BOLD, 20));
            setBorder(BorderFactory.createEmptyBorder(10, 5, 5, 5));
            //if (!list.getValueIsAdjusting()) {
            // BGs
            if (isSelected) {
                setBackground(Color.LIGHT_GRAY);
                //setForeground(Color.BLACK);
            } else {
                setBackground(Color.WHITE);
                //setForeground(Color.BLACK);
            }
            //}
            return this;
        }
    }
}
