package team7.cu.sims;

import team7.cu.utils.Helper;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;


public class Faculty {
    public String name;
    private int id;

    public Faculty(String name) {
        this.name = name;
    }

    public synchronized static ArrayList<Faculty> getAll() {
        ArrayList<Faculty> facs = new ArrayList<>();
        Connection connection = Helper.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultset = statement.executeQuery("SELECT  * FROM  faculties ORDER BY `name`");
            while (resultset.next()) {
                facs.add(buildFrom(resultset));
            }
            resultset.close();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return facs;
    }

    private static Faculty buildFrom(ResultSet resultSet) throws SQLException {
        Faculty Faculty = new Faculty(resultSet.getString("name"));
        Faculty.id = resultSet.getInt("fac_id");
        return Faculty;
    }

    public synchronized static Faculty getById(int id) {
        try {
            Connection connection = Helper.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM faculties WHERE fac_id = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            if (set.first()) {
                Faculty faculty = buildFrom(set);
                set.close();
                statement.close();
                return faculty;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static synchronized Faculty getByName(String name) {
        for (Faculty faculty : getAll())
            if (faculty.name.equalsIgnoreCase(name)) return faculty;
        return null;
    }

    public static void delete(HashSet<Integer> selectedFacIds) {
        Connection connection = Helper.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM faculties WHERE fac_id = ?");
            for (Integer id : selectedFacIds) {
                statement.setInt(1, id);
                statement.execute();
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public synchronized void save() {
        try {
            Connection connection = Helper.getConnection();
            if (id == 0) {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO faculties (`name`) VALUES (?)");
                statement.setString(1, name);
                statement.execute();
                statement.close();
            } else {
                PreparedStatement statement = connection.prepareStatement("UPDATE faculties SET `name` = ? WHERE fac_id = ?");
                statement.setString(1, name);
                statement.setInt(2, id);
                statement.execute();
                statement.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
