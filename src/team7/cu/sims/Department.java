package team7.cu.sims;

import team7.cu.utils.Helper;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;


public class Department {
    public String fullName, shortName;
    private int id;
    public Faculty faculty;

    public Department(String full_name, String short_name) {
        fullName = full_name;
        shortName = short_name;
    }

    public Department(String fullName) {
        this(fullName, null);
    }

    public synchronized static ArrayList<Department> getAll() {
        ArrayList<Department> depts = new ArrayList<>();
        Connection connection = Helper.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultset = statement.executeQuery("SELECT * FROM  departments ORDER BY full_name");
            while (resultset.next()) {
                depts.add(buildFrom(resultset));
            }
            resultset.close();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return depts;
    }

    private static Department buildFrom(ResultSet resultSet) throws SQLException {
        Department department = new Department(resultSet.getString("full_name"), resultSet.getString("short_name"));
        department.id = resultSet.getInt("dept_id");
        department.faculty = Faculty.getById(resultSet.getInt("fac_id"));
        return department;
    }

    public synchronized static Department getById(int id) {
        try {
            Connection connection = Helper.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM departments WHERE dept_id = ?");
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            if (set.first()) {
                Department department = buildFrom(set);
                set.close();
                statement.close();
                return department;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static synchronized Department getByName(String name) {
        for (Department department : getAll())
            if (department.fullName.equalsIgnoreCase(name)) return department;
        return null;
    }

    public static void delete(HashSet<Integer> selectedDeptIds) {
        Connection connection = Helper.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM departments WHERE dept_id = ?");
            for (Integer id : selectedDeptIds) {
                statement.setInt(1, id);
                statement.execute();
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public synchronized void save() {
        try {
            Connection connection = Helper.getConnection();
            if (id == 0) {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO departments (full_name, short_name, fac_id) VALUES (?, ?, ?)");
                statement.setString(1, fullName);
                statement.setString(2, shortName);
                statement.setString(3, String.valueOf(faculty.getId()));
                statement.execute();
                statement.close();
            } else {
                PreparedStatement statement = connection.prepareStatement("UPDATE departments SET full_name = ?, short_name = ?, fac_id = ? WHERE dept_id = ?");
                statement.setString(1, fullName);
                statement.setString(2, shortName);
                statement.setString(3, String.valueOf(faculty.getId()));
                statement.setInt(4, id);
                statement.execute();
                statement.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        if (shortName == null || shortName.isEmpty()) return fullName;
        return fullName + " (" + shortName + ")";
    }
}
