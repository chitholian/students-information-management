package team7.cu.sims;

import team7.cu.comps.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Chitholian on 6/23/2017.
 */
public class AddFacForm extends MyPanel {
    private MyTextField facName;
    private Faculty faculty;

    public AddFacForm() {
        super("Add New Faculty");
        decorate();
    }

    @Override
    public void decorate() {
        removeAll();
        gc = getDefaultConstraints();
        gc.anchor = GridBagConstraints.LINE_START;
        insert(new MyLabel("Name", "Full Name of the Faculty", null));
        gc.gridx = 1;
        facName = new MyTextField();
        insert(facName);

        MyButton submitBtn = new MyButton("Submit");
        gc.gridy++;
        submitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        String fName = facName.getText().trim();
                        if (fName.isEmpty()) {
                            Dialogs.alert(submitBtn, "Please enter faculty name");
                            facName.requestFocusInWindow();
                            return;
                        }
                        if (Dialogs.confirm(submitBtn, "Are you sure to submit ?")) {
                            if (faculty == null)
                                faculty = new Faculty(fName);
                            else {
                                faculty.name = fName;
                            }
                            if (faculty.getId() == 0 && Faculty.getByName(fName) != null && !Dialogs.confirm(submitBtn, "Faculty with this name already exists. Add Duplicate ?"))
                                return;
                            faculty.save();
                            AddFacForm.super.notifyDataChanged(new DataChangeOption(DataChangeOption.FACULTY, System.currentTimeMillis()));
                            // ((CDialog) container).notifyDataUpdated();
                            if (faculty.getId() == 0)
                                Dialogs.alert(submitBtn, "Operation Successful !");
                            else if (container instanceof Dialogs.Builder) ((Dialogs.Builder) container).dispose();
                        }
                    }
                });
            }
        });
        insert(submitBtn);
    }

    public void populateFrom(Faculty faculty) {
        facName.setText(faculty.name);
        this.faculty = faculty;
        title = "Edit - " + faculty.name;
    }
}
